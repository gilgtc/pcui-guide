# -*- coding: utf-8 -*-
"""
Created on Fri Oct  2 08:58:39 2015

%
% Filename    : post_process_pcui.py
% Author      : Goncalo Gil
% Description : Reads in PCUI data and plots an animation of the velocity field
%               for the cavity driven lid test run.
%               Also saves the animation in a PNG file sequence that can be put
%               into a GIF
%
% Author      : Goncalo Gil, Stanford University
% email       : gilg@stanford.edu
%

@author: gilg
"""

def read_pcui(fn,isvector,isghost,istep,skipx,skipy,skipz,params):

    #
    # Description : Reads in data output by PCUI. Expects unformatted Fortran
    #               binary files. It reads each file created by a particular
    #               processor and assembles the data into a single Matlab
    #               array.
    #
    #             - If isvector is equal to 0 it reads in three-dimensional
    #               arrays (i.e. density field)
    #
    #             - If isvector is equal to 1 it reads in four-dimensional
    #               arrays (i.e. grid and velocity field field)
    #
    #             - The istep variable indicates the time step at which the
    #               information is extracted. If the grid (x,y,z) is requested,
    #               then set istep = 1.
    #
    #             - If isghost = 1 then it extracts all the ghost points for each
    #               submatrix. If isghost = 0 then it only extracts the values of
    #               the array at the physical grid points.
    #

    def getdata(fn,count,fseek):
        import os
        import numpy as np

        with open(fn, 'rb') as f:
            # Skips FORTRAN binary file first 4 bytes (header)
            f.seek(fseek+4,os.SEEK_SET)
            data = np.fromfile(f, dtype=np.float, count=count)
    #    f.seek(4,os.SEEK_SET)  # Skips FORTRAN binary file first 4 bytes (header)
    #    data = np.fromfile(f, dtype=np.float)
        return data

    import numpy as np

    # processors
    px = int(read_input('px','../size.inc'))
    py = int(read_input('py','../size.inc'))
    pz = int(read_input('pz','../size.inc'))
    ni = int(read_input('ni','../size.inc'))
    nj = int(read_input('nj','../size.inc'))
    nk = int(read_input('nk','../size.inc'))
    nni = ni/px+4
    nnj = nj/py+4
    nnk = nk/pz+4

    N = nni * nnj * nnk

    if isvector:
        ptrD = 8 * (3 * N + 1)
    else:
        ptrD = 8 * (N + 1)

    for indxz in range(1,pz+1):
        for indxy in range(1,py+1):
            for indxx in range(1,px+1):
                coord =  str(500 + (indxx - 1) * pz * py + \
                        (indxy - 1) * pz + indxz - 1)
                fnt = fn + '.' + coord
                fseek = ptrD * (istep - 1)

                if isvector:
                    data = getdata(fnt, 3 * N, fseek)
                    data = data.reshape((nni,nnj,nnk,3),order = "FORTRAN")
                    if isghost:
                        x = data[:,:,:,0]
                        y = data[:,:,:,1]
                        z = data[:,:,:,2]
                    else:
                        x = data[2:-2,2:-2,2:-2,0]
                        y = data[2:-2,2:-2,2:-2,1]
                        z = data[2:-2,2:-2,2:-2,2]
                    x = x[0::skipx,0::skipy,0::skipz]
                    y = y[0::skipx,0::skipy,0::skipz]
                    z = z[0::skipx,0::skipy,0::skipz]
                else:
                    data = getdata(fnt,N,fseek)
                    data = data.reshape((nni,nnj,nnk),order = "FORTRAN")
                    if isghost:
                        s = data
                    else:
                        s = data[2:-2,2:-2,2:-2]
                    s = s[0::skipx,0::skipy,0::skipz]

                if indxx == 1:
                    if isvector:
                        X = x
                        Y = y
                        Z = z
                    else:
                        S = s
                else:
                    if isvector:
                        X = np.concatenate((X,x),axis=0)
                        Y = np.concatenate((Y,y),axis=0)
                        Z = np.concatenate((Z,z),axis=0)
                    else:
                        S = np.concatenate((S,s),axis=0)

            if indxy == 1:
                if isvector:
                    XX = X
                    YY = Y
                    ZZ = Z
                else:
                    SS = S
            else:
                if isvector:
                    XX = np.concatenate((XX,X),axis=1)
                    YY = np.concatenate((YY,Y),axis=1)
                    ZZ = np.concatenate((ZZ,Z),axis=1)
                else:
                    SS = np.concatenate((SS,S),axis=1)

        if indxz == 1:
            if isvector:
                XXX = XX
                YYY = YY
                ZZZ = ZZ
            else:
                SSS = SS
        else:
            if isvector:
                XXX = np.concatenate((XXX,XX),axis=2)
                YYY = np.concatenate((YYY,YY),axis=2)
                ZZZ = np.concatenate((ZZZ,ZZ),axis=2)
            else:
                SSS = np.concatenate((SSS,SS),axis=2)

    if isvector:
        x = XXX
        y = YYY
        z = ZZZ
        return x,y,z
    else:
        x = SSS
        return x

def read_input(var,fn):
        import re

        with open(fn, 'r+') as f:
            ftext = f.read()
        expr = var+r'\s*=\s*([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)'
        m = re.search(expr, ftext)
        if m:
            result = m.group().split()[-1]
        else:
            import sys
            sys.exit('No match for %s' % var)

        return result.replace('D','E')

import pylab as plt
import numpy as np
import os

# Set time frame for animation. ex: [0.5, 1.0] is from middle to end of simulation.
#                                   [0., 1.0] is from start to end of simulation.
make_animation = 1
animation_time = [0.,1.]
# Can skip temporal and spatial steps to allow for better performance when the
# grid size is large or there are too many frames saved
skipx = 1
skipy = 1
skipz = 1
skipt = 1

# Relevant PCUI data files
fn_xyz = '../xyz'
fn_output_UVW = '../output_UVW'
fn_output_S = '../output_S'

# animation files
fn_savefig = 'anim'

# processors
px = int(read_input('px','../size.inc'))
py = int(read_input('py','../size.inc'))
pz = int(read_input('pz','../size.inc'))
# domain
bx = float(read_input('bx','../cavity.f'))
by = float(read_input('by','../cavity.f'))
bz = float(read_input('bz','../cavity.f'))
ni = int(read_input('ni','../size.inc'))
nj = int(read_input('nj','../size.inc'))
nk = int(read_input('nk','../size.inc'))
nni = ni/px+4
nnj = nj/py+4
nnk = nk/pz+4
params = [px,py,pz,nni,nnj,nnk]
# time settings
nstep = int(read_input('nstep','../io.f'))
nsave = int(read_input('nsave','../io.f'))
dtime = float(read_input('dtime','../io.f'))
# Find out current size of file
byte_size = (nni*nnj*nnk*3 + 1)*8
stat_info = os.stat(fn_output_UVW + '.500')
c_file_size = stat_info.st_size
c_istep = int(c_file_size/byte_size)

# Actual number of time steps taken
nsteps = nstep/nsave + 1
if animation_time[0] == 0.:
    istepi = 2
else:
    istepi = int(animation_time[0]*nsteps)
    if istepi == 0:
        istepi = 2

istepf = int(animation_time[1]*nsteps)
if istepf == 0:
        istepf = 2

# This is the size of the resultant file given the animation_time[1]
tf_file_size = byte_size*istepf

# Deals with cases where the files were saved before the simulation was complete
if c_file_size < tf_file_size:
    istepf = c_istep
nsteps = istepf-istepi

# Read in x,y,z,u,v,w,rho (although rho is zero for cavity flow, iscalar = 0)

x, y, z = read_pcui(fn_xyz,1,0,1,skipx,skipy,skipz,params)
kslice = int(nk/10*0.5-1)
x = x[:,:,kslice].T
y = y[:,:,kslice].T

u, v, w = read_pcui(fn_output_UVW,1,0, istepi,skipx,skipy,skipz,params)
u = u[:,:,kslice].T
v = v[:,:,kslice].T
w = w[:,:,kslice].T
rho = read_pcui(fn_output_S, 0, 0, istepi,skipx,skipy,skipz,params)
rho = rho[:,:,kslice].T

title = (['u','v',r'$\rho$','quiver'])

nlevels = 10
lw = 1

plt.close('all')
figr = plt.figure(1, figsize=(8, 8))
plt.rc('font', family='serif')
plt.rc('font', family='serif')
plt.rc('xtick', labelsize='small')
plt.rc('ytick', labelsize='small')

plt.subplot(221)
im1 = plt.imshow(u,cmap='RdBu',origin='lower',interpolation="nearest", \
                aspect='auto',extent=[x.min(), x.max(), y.min(), y.max()])
levels = np.linspace(u.min(),u.max(),nlevels)
#cset1 = plt.contour(u,levels,linewidths=lw,extent=[x.min(),x.max(),y.min(),y.max()])
#plt.clabel(cset1,inline=True,fmt='%3.2f',fontsize=10)
plt.title(title[0])
plt.ylabel(r'$z \, (m)$')

plt.subplot(222)
im2 = plt.imshow(v,cmap='RdBu',origin='lower',interpolation="nearest", \
                aspect='auto',extent=[x.min(), x.max(), y.min(), y.max()])
levels = np.linspace(v.min(),v.max(),nlevels)
#cset2 = plt.contour(v,levels,linewidths=lw,extent=[x.min(),x.max(),y.min(),y.max()])
#plt.clabel(cset2,inline=True,fmt='%3.2f',fontsize=10)
plt.title(title[1])

plt.subplot(223)
im3 = plt.imshow(rho,cmap='RdBu',origin='lower',interpolation="nearest", \
                aspect='auto',extent=[x.min(), x.max(), y.min(), y.max()])
levels = np.linspace(rho.min(),rho.max(),nlevels)
#cset3 = plt.contour(rho,levels,linewidths=lw,extent=[x.min(),x.max(),y.min(),y.max()])
#plt.clabel(cset3,inline=True,fmt='%3.2f',fontsize=10)
plt.title(title[2])
plt.xlabel(r'$x \, (m)$')
plt.ylabel(r'$z \, (m)$')

plt.subplot(224)
plt.title(title[3])
plt.xlabel(r'$x \, (m)$')

lw = 0.8
hw = 1.
w = 0.001
im4 = plt.quiver(x,y,u,v,scale=1,linewidths=lw,width=w,headwidth=hw)

num = 0
for i in range(istepi+1,istepf+1,skipt):
    num += 1
    u, v, w = read_pcui(fn_output_UVW,1,0, i,skipx,skipy,skipz,params)
    u = u[:,:,kslice].T
    v = v[:,:,kslice].T
    w = w[:,:,kslice].T
    rho = read_pcui(fn_output_S, 0, 0, i,skipx,skipy,skipz,params)
    rho = rho[:,:,kslice].T
    #print np.max(u)
    im1.set_data(u)
    im2.set_data(v)
    im3.set_data(rho)
    im4.set_UVC(u,v)

    plt.pause(0.0001)
    figr.canvas.draw()

    # save each frame into a PNG file
    #   - increase dpi for better resolution (but may make convert very slow)
    if make_animation:
        plt.savefig(fn_savefig + '_' + format(num,'05') + '.png',bbox_inches='tight',dpi=100)

if  c_file_size < tf_file_size:
    print('The final time step in the range %s is too large. So far only %s timesteps were processed.' % (istepf, c_istep))