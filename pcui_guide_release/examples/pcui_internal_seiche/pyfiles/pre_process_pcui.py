# -*- coding: utf-8 -*-
"""
Created on Tue Oct  6 19:50:47 2015

#
# Filename    : initialize_pcui.m
# Author      : Goncalo Gil
# Description : Creates an input file to be read by PCUI. It writes
#               2D arrays (but can easily be extended to higher dimensions)
#
# Author      : Goncalo Gil, Stanford University
# email       : gilg@stanford.edu
#

@author: gilg
"""

def read_pcui(fn,isvector,isghost,istep,skipx,skipy,skipz):

    #
    # Description : Reads in data output by PCUI. Expects unformatted Fortran
    #               binary files. It reads each file created by a particular
    #               processor and assembles the data into a single Matlab
    #               array.
    #
    #             - If isvector is equal to 0 it reads in three-dimensional
    #               arrays (i.e. density field)
    #
    #             - If isvector is equal to 1 it reads in four-dimensional
    #               arrays (i.e. grid and velocity field field)
    #
    #             - The istep variable indicates the time step at which the
    #               information is extracted. If the grid (x,y,z) is requested,
    #               then set istep = 1.
    #
    #             - If isghost = 1 then it extracts all the ghost points for each
    #               submatrix. If isghost = 0 then it only extracts the values of
    #               the array at the physical grid points.
    #

    def getdata(fn,count,fseek):
        import os
        import numpy as np

        with open(fn, 'rb') as f:
            # Skips FORTRAN binary file first 4 bytes (header)
            f.seek(fseek+4,os.SEEK_SET)
            data = np.fromfile(f, dtype=np.float, count=count)
    #    f.seek(4,os.SEEK_SET)  # Skips FORTRAN binary file first 4 bytes (header)
    #    data = np.fromfile(f, dtype=np.float)
        return data

    import numpy as np

    # processors
    px = int(read_input('px','../size.inc'))
    py = int(read_input('py','../size.inc'))
    pz = int(read_input('pz','../size.inc'))
    ni = int(read_input('ni','../size.inc'))
    nj = int(read_input('nj','../size.inc'))
    nk = int(read_input('nk','../size.inc'))
    nni = ni/px+4
    nnj = nj/py+4
    nnk = nk/pz+4

    N = nni * nnj * nnk

    if isvector:
        ptrD = 8 * (3 * N + 1)
    else:
        ptrD = 8 * (N + 1)

    for indxz in range(1,pz+1):
        for indxy in range(1,py+1):
            for indxx in range(1,px+1):
                coord =  str(500 + (indxx - 1) * pz * py + \
                        (indxy - 1) * pz + indxz - 1)
                fnt = fn + '.' + coord
                fseek = ptrD * (istep - 1)

                if isvector:
                    data = getdata(fnt, 3 * N, fseek)
                    data = data.reshape((nni,nnj,nnk,3),order = "FORTRAN")
                    if isghost:
                        x = data[:,:,:,0]
                        y = data[:,:,:,1]
                        z = data[:,:,:,2]
                    else:
                        x = data[2:-2,2:-2,2:-2,0]
                        y = data[2:-2,2:-2,2:-2,1]
                        z = data[2:-2,2:-2,2:-2,2]
                    x = x[0::skipx,0::skipy,0::skipz]
                    y = y[0::skipx,0::skipy,0::skipz]
                    z = z[0::skipx,0::skipy,0::skipz]
                else:
                    data = getdata(fnt,N,fseek)
                    data = data.reshape((nni,nnj,nnk),order = "FORTRAN")
                    if isghost:
                        s = data
                    else:
                        s = data[2:-2,2:-2,2:-2]
                    s = s[0::skipx,0::skipy,0::skipz]

                if indxx == 1:
                    if isvector:
                        X = x
                        Y = y
                        Z = z
                    else:
                        S = s
                else:
                    if isvector:
                        X = np.concatenate((X,x),axis=0)
                        Y = np.concatenate((Y,y),axis=0)
                        Z = np.concatenate((Z,z),axis=0)
                    else:
                        S = np.concatenate((S,s),axis=0)

            if indxy == 1:
                if isvector:
                    XX = X
                    YY = Y
                    ZZ = Z
                else:
                    SS = S
            else:
                if isvector:
                    XX = np.concatenate((XX,X),axis=1)
                    YY = np.concatenate((YY,Y),axis=1)
                    ZZ = np.concatenate((ZZ,Z),axis=1)
                else:
                    SS = np.concatenate((SS,S),axis=1)

        if indxz == 1:
            if isvector:
                XXX = XX
                YYY = YY
                ZZZ = ZZ
            else:
                SSS = SS
        else:
            if isvector:
                XXX = np.concatenate((XXX,XX),axis=2)
                YYY = np.concatenate((YYY,YY),axis=2)
                ZZZ = np.concatenate((ZZZ,ZZ),axis=2)
            else:
                SSS = np.concatenate((SSS,SS),axis=2)

    if isvector:
        x = XXX
        y = YYY
        z = ZZZ
        return x,y,z
    else:
        x = SSS
        return x

def read_input(var,fn):
        import re

        with open(fn, 'r+') as f:
            ftext = f.read()
        expr = var+r'\s*=\s*([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)'
        m = re.search(expr, ftext)
        if m:
            result = m.group().split()[-1]
        else:
            import sys
            sys.exit('No match for %s' % var)

        return result.replace('D','E')

def assemble_array_2d(B):

#
# Filename    : assemble_array_2d.m
# Author      : Goncalo Gil
# Description : Assembles an array (2-D) into a PCUI ready set
#               of binary files depending on the number of processors
#               requested. Essentially it takes an array
#               with no ghostpoints and creates a series of subarrays with
#               ghostpoints as required by PCUI. The subarrays are then
#               assembled into an encompassing array A.
#
# Author      : Goncalo Gil, Stanford University
# email       : gilg@stanford.edu
#
    import numpy as np

    px = int(read_input('px','../size.inc'))
    py = int(read_input('py','../size.inc'))
    pz = int(read_input('pz','../size.inc'))
    ni = int(read_input('ni','../size.inc'))
    nj = int(read_input('nj','../size.inc'))

    nni_A = ni/px+4
    nnj_A = nj/py+4
    nni_B = nni_A-4
    nnj_B = nnj_A-4

    A = np.zeros((nni_A*px,nnj_A*py))

    for indxz in range(1,pz+1):
        for indxy in range(1,py+1):
            for indxx in range(1,px+1):

                # Interior (i)
                # X direction -> B = Back F = Front
                ix_A = np.arange((indxx-1) * nni_A+2,indxx * nni_A - 2)
                ix_B = np.arange((indxx-1) * nni_B,indxx * nni_B)
                B_A  = np.arange((indxx-1) * nni_A,(indxx-1) * nni_A + 2)
                F_A  = np.arange(indxx * nni_A-2,indxx * nni_A)

                # Deal with special cases
                if indxx == 1 and indxx == px:
                    B_B = np.array([1, 0])
                    F_B = np.array([indxx * nni_B - 1, indxx * nni_B - 2])
                elif indxx == 1:
                    B_B = np.array([1, 0])
                    F_B = np.array([indxx * nni_B, indxx * nni_B + 1])
                elif indxx == px:
                    B_B = np.array([(indxx-1)*nni_B-2, (indxx-1)*nni_B-1])
                    F_B = np.array([indxx * nni_B-1, indxx * nni_B - 2])
                else:
                    B_B = np.arange((indxx-1) * nni_B-2,(indxx-1) * nni_B)
                    F_B = np.arange(indxx * nni_B,indxx * nni_B + 2)


                # Y direction -> L = Left R = Right
                iy_A = np.arange((indxy-1) * nnj_A+2,indxy * nnj_A - 2)
                iy_B = np.arange((indxy-1) * nnj_B,indxy * nnj_B)
                L_A =  np.arange((indxy-1) * nnj_A,(indxy-1) * nnj_A + 2)
                R_A =  np.arange(indxy * nnj_A-2,indxy * nnj_A)

                # Deal with special cases
                if indxy == 1 and indxy == py:
                    L_B = np.array([1, 0])
                    R_B = np.array([indxy * nnj_B - 1, indxy * nnj_B - 2])
                elif indxy == 1:
                    L_B = np.array([1, 0])
                    R_B = np.array([indxy * nnj_B, indxy * nnj_B + 1])
                elif indxy == py:
                    L_B = np.array([(indxy-1)*nnj_B-2, (indxy-1) * nnj_B-1])
                    R_B = np.array([indxy * nnj_B-1, indxy * nnj_B - 2])
                else:
                    L_B = np.arange((indxy-1) * nnj_B-2,(indxy-1) * nnj_B)
                    R_B = np.arange(indxy * nnj_B,indxy * nnj_B + 2)

                # Kernel [1]
                A[np.ix_(ix_A,iy_A)] = B[np.ix_(ix_B,iy_B)].copy()

                # Corners [8]
                A[np.ix_(B_A,L_A)] = B[np.ix_(B_B,L_B)].copy()
                A[np.ix_(F_A,L_A)] = B[np.ix_(F_B,L_B)].copy()
                A[np.ix_(B_A,R_A)] = B[np.ix_(B_B,R_B)].copy()
                A[np.ix_(F_A,R_A)] = B[np.ix_(F_B,R_B)].copy()
                A[np.ix_(B_A,L_A)] = B[np.ix_(B_B,L_B)].copy()
                A[np.ix_(F_A,L_A)] = B[np.ix_(F_B,L_B)].copy()
                A[np.ix_(B_A,R_A)] = B[np.ix_(B_B,R_B)].copy()
                A[np.ix_(F_A,R_A)] = B[np.ix_(F_B,R_B)].copy()

                # Columns [12]
                A[np.ix_(ix_A,L_A)] = B[np.ix_(ix_B,L_B)].copy()
                A[np.ix_(ix_A,L_A)] = B[np.ix_(ix_B,L_B)].copy()
                A[np.ix_(ix_A,R_A)] = B[np.ix_(ix_B,R_B)].copy()
                A[np.ix_(ix_A,R_A)] = B[np.ix_(ix_B,R_B)].copy()
                A[np.ix_(B_A,iy_A)] = B[np.ix_(B_B,iy_B)].copy()
                A[np.ix_(B_A,iy_A)] = B[np.ix_(B_B,iy_B)].copy()
                A[np.ix_(F_A,iy_A)] = B[np.ix_(F_B,iy_B)].copy()
                A[np.ix_(F_A,iy_A)] = B[np.ix_(F_B,iy_B)].copy()
                A[np.ix_(B_A,L_A )] = B[np.ix_(B_B,L_B)].copy()
                A[np.ix_(F_A,L_A )] = B[np.ix_(F_B,L_B)].copy()
                A[np.ix_(B_A,R_A )] = B[np.ix_(B_B,R_B)].copy()
                A[np.ix_(F_A,R_A )] = B[np.ix_(F_B,R_B)].copy()

                # Sides [6]

                A[np.ix_(ix_A,iy_A)] = B[np.ix_(ix_B,iy_B)].copy()
                A[np.ix_(ix_A,iy_A)] = B[np.ix_(ix_B,iy_B)].copy()
                A[np.ix_(B_A,iy_A )] = B[np.ix_(B_B ,iy_B)].copy()
                A[np.ix_(F_A,iy_A )] = B[np.ix_(F_B ,iy_B)].copy()
                A[np.ix_(ix_A,L_A )] = B[np.ix_(ix_B,L_B)].copy()
                A[np.ix_(ix_A,R_A )] = B[np.ix_(ix_B,R_B)].copy()

    return A

def write_pcui(fn,A):

    def putdata(fn,data):

    #
    # Description : Reads data from a FORTRAN binary file
    #
        with open(fn, 'wb') as f:
            f.write('aaaa') # Skips FORTRAN binary file first 4 bytes (header)
            data.tofile(f, format='%d')
            f.write('bbbb') # Skips FORTRAN binary file first 4 bytes (footer)

        return data

    # processors
    px = int(read_input('px','../size.inc'))
    py = int(read_input('py','../size.inc'))
    pz = int(read_input('pz','../size.inc'))

    # domain
    ni = int(read_input('ni','../size.inc'))
    nj = int(read_input('nj','../size.inc'))

    nni = ni/px+4
    nnj = nj/py+4

    for indxz in range(1,pz+1):
        for indxy in range(1,py+1):
            for indxx in range(1,px+1):
                ff  = fn + '.' + str(500+(indxx-1)*pz*py+(indxy-1)*pz+indxz-1)
                putdata(ff,A[(indxx-1)*nni:indxx*nni, \
                             (indxy-1)*nnj:indxy*nnj])

def ndgrid(x,y):
    import numpy as np

    X,Y = np.meshgrid(x,y)
    X = np.transpose(X)
    Y = np.transpose(Y)

    return X,Y

# domain
ni = int(read_input('ni','../size.inc'))
nj = int(read_input('nj','../size.inc'))
bx = float(read_input('bx','../cavity.f'))
by = float(read_input('by','../cavity.f'))

fn_write_u = 'u_init'
fn_write_v = 'v_init'
fn_write_rho_init = 'rho_init'
fn_write_rho_full = 'rho_full'

# These are the output files with the filenames stripped out of extensions
# (extensions are chosen automatically based on number of processors).
fn_xyz = '../xyz'
fn_rho = '../input_S'
fn_uvw = '../input_UVW'
fn_rho_init_to_PCUI = '../rho_init_from_matlab'
fn_rho_full_to_PCUI = '../rho_full_from_matlab'

# -------------------------------------------------------------------------
# Get problem parameters and variables from PCUI
# -------------------------------------------------------------------------
# read the file containing the parameter definition
dt = float(read_input('dtime','../io.f'))
molecular_viscosity = float(read_input('vis','../io.f'))
eddy_viscosity = float(read_input('ak','../io.f'))
nstep = int(read_input('nstep','../io.f'))
nsave = int(read_input('nsave','../io.f'))

# read the file containing the domain definition
bx = float(read_input('bx','../cavity.f'))
by = float(read_input('by','../cavity.f'))
bz = float(read_input('bz','../cavity.f'))

# read the file containing the grid size and processor definitions
px = int(read_input('px','../size.inc'))
py = int(read_input('py','../size.inc'))
pz = int(read_input('pz','../size.inc'))
ni = int(read_input('ni','../size.inc'))
nj = int(read_input('nj','../size.inc'))
nk = int(read_input('nk','../size.inc'))
nni = ni/px+4
nnj = nj/py+4
nnk = nk/pz+4
# read the files containing the grid definition and assemble into array
# (includes ghost grid points of each submatrix)
skipx = 1
skipy = 1
skipz = 1
kslice = int(nk*0.5-1)

x, y, z = read_pcui(fn_xyz,1,0,1,skipx,skipy,skipz)
x = x[:,:,kslice]
y = y[:,:,kslice]
# -------------------------------------------------------------------------
# Initialize PCUI with an internal seiche
# -------------------------------------------------------------------------
# Prepare density and velocity field
D = by
rho_init_pcui = np.ones((ni,nj))
rho_pert_pcui = np.zeros((ni,nj))
rho_pert_pcui = -0.3*0.06*np.tanh(.9*(y-0.1*np.cos(np.pi*x)-D/2.)/0.2 \
                *np.arctanh(0.99))
rho_full_pcui = rho_init_pcui+rho_pert_pcui

# Write PCUI binary files depending on the number of processors
write_pcui(fn_rho_init_to_PCUI,assemble_array_2d(rho_init_pcui))
write_pcui(fn_rho_full_to_PCUI,assemble_array_2d(rho_full_pcui))

#
# -------------------------------------------------------------------------
# Verify initialized internal seiche
# -------------------------------------------------------------------------
# Plot density field
plt.close('all')
plt.figure(1)
plt.imshow(rho_full_pcui.T)