# -*- coding: utf-8 -*-
"""

@author: gilg

%
% Filename    : create_gif.py
% Author      : Goncalo Gil
% Description : Uses imagemagick to assemble a set of PNG frames into a GIF
%
% Author      : Goncalo Gil, Stanford University
% email       : gilg@stanford.edu
%

Must have imagemagick installed:

http://www.imagemagick.org/script/binary-releases.php

It must be run from the command line. Say you used the suffix 'anim' for each
of the PNG files. Then run this in a terminal:

python create_gif.py anim

This will then merge the files and create a good quality animated gif. It may take
some time depending on the size of the animation.

"""
import subprocess
import argparse

parser = argparse.ArgumentParser(description='ADD YOUR DESCRIPTION HERE')
parser.add_argument('filename',type=str)
argsIN = parser.parse_args()

argsOUT = (['convert', '-monitor', '-delay', '10', '-dispose', 'Background', '+page',
            '+dither', argsIN.filename + '_*.png',
            '-loop', '0', argsIN.filename + '.gif'])

subprocess.call(argsOUT)