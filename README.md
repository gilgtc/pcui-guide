# README: Quick PCUI Guide
=========================================

PCUI is a Large-Eddy Simulation parallel code developed at Stanford University.
It is a "Non-staggered grid, fractional step method for time-dependent
incompressible navier-stokes equations in curvilinear coordinates."
(Zang et al 1994, Journal of Computational Physics)

This repo contains all the parts to the guide, including the LES numerical code,
the post-processing Matlab code and latex documents.

Read the [PCUI Guide](https://bitbucket.org/gilgtc/pcui-guide/raw/4adbc3744375b0da573f9b6ab7ea0e8000fa76dd/pcui_guide.pdf)
located in the source and follow the examples to get started.

Here are some examples using animated GIFs.

Cavity flow
http://tinyurl.com/p63qfuw

Internal seiche
http://tinyurl.com/nsgtljy