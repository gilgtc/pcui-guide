\chapter{PCUI structure}
This chapter deals with the structure of PCUI. 
First, all files are listed with their summaries, then the main
file \textit{ns.f} and initialization file \textit{init.f} are briefly
described.
Next, we categorize and list the most important variables and provide a summary
for each one.

\section{File summary}

\subsection{Runtime files}
\begin{center}
    \begin{tabular}{| l | l | l |}
    \hline
    \textbf{Nr.} &\textbf{Filename} & \textbf{Summary} \\ \hline
    01 & Makefile & Makefile to organize code compilation \\ \hline       
    02 & cav      & Resulting executable from running \textit{make}
\\ \hline
    \end{tabular}
\end{center}

\subsection{Fortran files}
Here the first table shows the files that contain the subroutines called by the
main subroutine (\textit{ns.f}).
The second table contains files with auxiliary subroutines.

\begin{table}[h]
    \centering
    \caption{Main files}
    \begin{tabular}{| l | l | l |}
    \hline
    \textbf{Nr.} &\textbf{Filename} & \textbf{Summary} \\ \hline
    01 & ns.f     & Main file: run the code \\ \hline    
    02 & mp.f     & Initialize MPI \\ \hline
    03 & init.f   & Initialize scalar and vector fields ($\rho$, $\mathbf{u}$)
\\ \hline
    04 & cavity.f & Sets up domain size and grid for Example 2 \\ \hline
    05 & isw.f    & Sets up domain size and grid for Example 3 \\ \hline      
    06 & io.f     & Deals with input and output \\ \hline
    07 & eddy.f   & LES model \\ \hline
    08 & scal.f   & Scalar solver \\ \hline
    09 & pred.f   & Predictor step \\ \hline
    10 & pres.f   & Pressure initialization \\ \hline
    11 & corr.f   & Corrector step (velocity boundary conditions are set here)
\\ \hline
    12 & grid\_init.f & Initializes grid and outputs to file \\ \hline
    \end{tabular}
\end{table}

\begin{table}[h]
    \centering
    \caption{Auxiliary files}
    \begin{tabular}{| l | l | l |}
    \hline
    \textbf{Nr.} &\textbf{Filename} & \textbf{Summary} \\ \hline   
    11 & trid.f   & Tridiagonal solver \\ \hline
    12 & smad.f   & ADI Smoothing \\ \hline
    13 & resd.f   & Residual \\ \hline
    14 & fctr.f   & Fine to coarse and coarse to fine \\ \hline
    15 & exch.f   & Exchange boundary values  \\ \hline
    16 & conv.f   & Explicit convective terms \\ \hline
    \end{tabular}
\end{table}

\subsection{Include files}
\begin{table}[h]
    \centering
    \caption{Include files}
    \begin{tabular}{| l | l | l |}
    \hline
    \textbf{Nr.} & \textbf{Filename} & \textbf{Summary} \\ \hline
    01 & cavity.inc & Define domain variables \\ \hline
    02 & eddy.inc   & Define LES model variables \\ \hline
    03 & metric.inc & Define metrics \\ \hline
    04 & mpi.inc    & Define MPI topology variables \\ \hline
    05 & ns.inc     & Define field variables\\ \hline
    06 & para.inc   & Define auxiliary variables and parameters \\ \hline
    07 & size.inc   & Define grid size and number of processors \\ \hline
    \end{tabular}
\end{table}

\subsection{Output files}
\begin{table}[h]
    \centering
    \caption{Output files}
    \begin{tabular}{| l | l | l |}
    \hline
    \textbf{Nr.} & \textbf{Filename} & \textbf{Summary} \\ \hline
    01 & xyz.\#\#\#        & Binary files containing grid \\ \hline
    02 & output\_UVW.\#\#\# & Binary files containing velocity vector field \\
\hline
    03 & output\_S.\#\#\#   & Binary files containing density scalar field \\
\hline
    04 & qoutput        & File that outputs run progress to assist when running
in Bobstar \\ \hline
    \end{tabular}
\end{table}

The \# symbol is a number that the file extension is given depending on the
processor number that saved that file.
\section{File Description}

\subsection{ns.f}
This is the main file that runs the code. It does the following:
\begin{itemize}
\item Initialize MPI.
\item Initialize all variables.
\item Initialize MPI topology.
\item Initialize domain and set up grid.
\item Iterate over the number of time steps. 
\begin{itemize}
\item Save all relevant data to binary files.
\item Call LES model.
\item Compute the right hand side of the scalar field and update it. 
\item Call predictor step, update intermediate velocity u*. 
\item Call multigrid pressure solver. 
\item Call corrector step, apply velocity boundary conditions. 
\item Call scalar field solver, apply scalar field boundary conditions.
\item Call CFL number checker.
\end{itemize}
\item Save last step.
\item Output statistics to screen.
\end{itemize}

\subsection{init.f}
This is where the velocity and scalar fields are defined.
This version of PCUI is set up to run the classic test case of a
three-dimensional lid driven cavity, you can find details in
\cite{cui1999thesis}.
The subroutine first initializes the lid velocity variable \textit{u\_lid}.
Next, it initializes the scalar and vector fields including boundary conditions.

\section{Variable summary}
Only the most relevant variables are shown here.
\begin{table}[h]
    \centering
    \caption{Parameters}
    \begin{tabular}{| l | l | l |}
    \hline
    \textbf{Nr.}  &\textbf{Filename} & \textbf{Summary} \\ \hline
    01 & dtime    & Time step \\ \hline    
    02 & case     & Chooses different lid velocities depending on value of
\textit{case} (check \textit{init.f}) \\ \hline
    03 & newrun   & Used to start a new run or to start off where
last run ended
\\ \hline
    04 & periodic & Chooses if periodic boundary conditions are required \\
\hline  
    05 & iscalar  & Chooses if scalar solver is executed\\ \hline    
    06 & mg\_level & Multigrid level \\ \hline
    07 & nstep    & Number of steps \\ \hline
    08 & nsave    & Step interval to save results \\ \hline
    09 & vis      & Molecular viscosity \\ \hline
    10 & ak       & Eddy viscosity \\ \hline  
    \end{tabular}
\end{table}

\begin{table}[h]
    \centering
    \caption{Field, domain and grid variables}
    \begin{tabular}{| l | l | l |}
    \hline
    \textbf{Nr.}  &\textbf{Filename} & \textbf{Summary} \\ \hline
    01 & phi      & Density scalar field\\ \hline    
    02 & phi\_init & Initial density scalar field \\ \hline
    03 & p        & Pressure scalar field \\ \hline
    04 & u        & Velocity vector field\\ \hline
    05 & u\_lid    & Lid velocity\\ \hline
    06 & xp       & Grid definition \\ \hline   
    07 & bx       & Domain length \\ \hline
    08 & by       & Domain height (note that y is the vertical coordinate and
points upwards) \\ \hline
    09 & bz       & Domain width \\ \hline
    10 & stretchx & Stretched grid in x-direction \\ \hline
    11 & stretchy & Stretched grid in y-direction \\ \hline
    12 & stretchz & Stretched grid in z-direction \\ \hline
    \end{tabular}
\end{table}